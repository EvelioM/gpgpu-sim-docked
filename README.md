# GPGPU-sim docked

A docker solution to ease GPGPU-sim setup.

## Using the image

I recommend building the image as such:

```sh
docker build -t $(basename $PWD) .
```

But you are free to name it however you want.

Run it with:

```sh
docker run --rm -it gpgpu-sim-docked
```

## To use GPGPU-sim

### Compiling your application

You need to compile your `cuda` application using:

```sh
nvcc --cudart shared -o <your_app_name> <your_source_file>.cu
```

### Setting up the environment

You need to do:

```sh
source /gpgpu_sim/gpgpu-sim_distribution/setup_environment release
```

This changes `LD_LIBRARY_PATH` so that your application uses the simulator as a library instead of the Nvidia drivers (there won't be any, and it will fail if you try to run it like that).

### Select a configuration

You must take a configuration for the simulation. For that you should copy all the files from one of the config directories provided to your workspace. For example:

```sh
cp /gpgpu_sim/gpgpu-sim_distribution/configs/tested-cfgs/SM2_GTX480/* ./
```

### Running the simulation

After all of that you can run the simulation by running the application as usual.
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cuda.h>
#include <cuda_runtime.h>
#include "device_launch_parameters.h"


int main()
{
	// Get number of devices
	int nDevices = 0;
	cudaError_t errorcode = cudaSuccess;
	errorcode = cudaGetDeviceCount(&nDevices);
	std::cout << "Number of CUDA GPUs: " << nDevices << std::endl;
	std::cout << cudaGetErrorString(errorcode) << std::endl;
	// Loop over all devices
	
	for(int i = 0; i < nDevices; i++)
	{	
        std::cout << "============================" << std::endl;
		cudaDeviceProp prop;
		cudaGetDeviceProperties(&prop, i);
		std::cout << "Device number: " << i << std::endl;
		std::cout << "Device name: " << prop.name << std::endl;
		std::cout << "Minor Compute capability: " << prop.minor << std::endl;
		std::cout << "Major compute capability: " << prop.major << std::endl;
		std::cout << "Memory Clock Rate (KHz): " << prop.memoryBusWidth << std::endl;
		std::cout << "Peak Memory Bandwidth (GB/s): " << 
			2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6 << std::endl;
		std::cout << "Maximum threads per block: "<< prop.maxThreadsPerBlock << std::endl;
		std::cout << "Maximum threads per MP: "<< prop.maxThreadsPerMultiProcessor << std::endl;
		std::cout << "Size of a warp: "<< prop.warpSize << std::endl;
		std::cout << "Available shared memory per block: " << std::setprecision (3) << (float)prop.sharedMemPerBlock/(1024) << " KB" << std::endl;
		std::cout << "Maximum global memory: " << std::setprecision (3) << (float)prop.totalGlobalMem/(1024*1024*1024) << " GB" << std::endl;
		std::cout << "Constant memory: " << std::setprecision (3) << (float)prop.totalConstMem/(1024) << " KB" << std::endl;
	}
	
	return 0;
}

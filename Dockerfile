FROM ubuntu:18.04

# Ask docker to use bash
SHELL ["/bin/bash", "-c"]

# Set GPU availability
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=8.0"

# Ubuntu needs this
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Madrid
RUN apt-get update -y && apt-get install -y tzdata

# Packages needed
RUN apt-get install -y --no-install-recommends git 
RUN apt-get install -y --no-install-recommends g++ 
RUN apt-get install -y --no-install-recommends g++-5 
RUN apt-get install -y --no-install-recommends gcc-5 
RUN apt-get install -y --no-install-recommends python 
RUN apt-get install -y --no-install-recommends python-pip 
RUN apt-get install -y --no-install-recommends build-essential 
RUN apt-get install -y --no-install-recommends checkinstall 
RUN apt-get install -y --no-install-recommends libreadline-gplv2-dev 
RUN apt-get install -y --no-install-recommends libncursesw5-dev 
RUN apt-get install -y --no-install-recommends libssl-dev 
RUN apt-get install -y --no-install-recommends libsqlite3-dev 
RUN apt-get install -y --no-install-recommends tk-dev 
RUN apt-get install -y --no-install-recommends libgdbm-dev 
RUN apt-get install -y --no-install-recommends libc6-dev 
RUN apt-get install -y --no-install-recommends libbz2-dev 
RUN apt-get install -y --no-install-recommends scons 
RUN apt-get install -y --no-install-recommends swig 
RUN apt-get install -y --no-install-recommends m4 
RUN apt-get install -y --no-install-recommends autoconf 
RUN apt-get install -y --no-install-recommends automake 
RUN apt-get install -y --no-install-recommends libtool 
RUN apt-get install -y --no-install-recommends curl 
RUN apt-get install -y --no-install-recommends make 
RUN apt-get install -y --no-install-recommends cmake 
RUN apt-get install -y --no-install-recommends unzip 
RUN apt-get install -y --no-install-recommends python-pydot 
RUN apt-get install -y --no-install-recommends flex 
RUN apt-get install -y --no-install-recommends bison 
RUN apt-get install -y --no-install-recommends xutils 
RUN apt-get install -y --no-install-recommends libx11-dev 
RUN apt-get install -y --no-install-recommends libxt-dev 
RUN apt-get install -y --no-install-recommends libxmu-dev 
RUN apt-get install -y --no-install-recommends libxi-dev 
RUN apt-get install -y --no-install-recommends libgl1-mesa-dev 
RUN apt-get install -y --no-install-recommends python-dev 
RUN apt-get install -y --no-install-recommends imagemagick 
RUN apt-get install -y --no-install-recommends libpng-dev 
RUN apt-get install -y --no-install-recommends gettext
RUN apt-get install -y --no-install-recommends openssh-client
RUN apt-get install -y --no-install-recommends wget
RUN apt-get install -y --no-install-recommends xutils-dev
RUN apt-get install -y --no-install-recommends zlib1g-dev
RUN apt-get install -y --no-install-recommends libglu1-mesa-dev
RUN apt-get install -y --no-install-recommends doxygen
RUN apt-get install -y --no-install-recommends graphviz

# Needed for git cloning
RUN apt-get install -y \
        ca-certificates \
        && update-ca-certificates

# We will need cuda
RUN mkdir /cuda && cd /cuda && \ 
    wget -q --no-check-certificate https://developer.nvidia.com/compute/cuda/9.1/Prod/local_installers/cuda_9.1.85_387.26_linux 

RUN \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 50 \
    && update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-5 50
RUN /bin/sh /cuda/cuda_9.1.85_387.26_linux --silent --toolkit --toolkitpath=/cuda
RUN rm -rf /cuda/cuda_9.1.85_387.26_linux 

# RUN \
#     update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 70 \
#     && update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-7 70

ENV CUDA_INSTALL_PATH=/cuda
ENV PATH="/cuda/bin:$PATH"
ENV LD_LIBRARY_PATH=/cuda/lib64

# We need mako for mesa compiling
RUN pip install mako

#Clone and build gpgpu-sim
RUN mkdir /gpgpu_sim && cd /gpgpu_sim && \
    git clone https://github.com/gpgpu-sim/gpgpu-sim_distribution.git

SHELL ["/bin/bash", "-c"] 
RUN cd /gpgpu_sim/gpgpu-sim_distribution && \
    source ./setup_environment release ; \
    make -j`nproc`

RUN mkdir /codes
COPY codes/* /codes/

WORKDIR /codes